# Healthspan Networks #

This repository presents the source code for the presentation of Senescence-associated
molecular interaction pathways. It is the supplement to a paper submitted in
2018 and shows on http://functional.domains/senescence/.

### How do I get set up? ###

For inspection, only a regular web browser is required. Download this repository via
the 'Downloads' button to the left. Unzip and open the index.html file in your browser.
An initial inspection may take a minute or two e.g. for the automated download of
the CytoscapeJS Java library

You will find the web page displaying five networks. Below every network, alphabetically
ordered the genes interacting are listed as buttons and so are the most abundant
Gene Ontology terms associated with these genes. A click on these buttons will
highlight the single gene or the set of genes in the graph.

### Contribution guidelines ###

Please create issues for any change you find helpful. And pull requests are welcome.

### Whom do I talk to? ###

Please contact Steffen Möller <steffen.moeller@uni-rostock.de> about the JavaScript
implementation, address Prof. Georg Fuellen about the pathways.
